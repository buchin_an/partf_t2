package com.company;

import com.company.model.Almanac;
import com.company.model.Book;
import com.company.model.Magazine;

public class Printer {
    public static void print(Book book) {
        System.out.println(book.getAuthor());
        System.out.println(book.getName());
        System.out.println(book.getPublishing());
        System.out.println(book.getYear());
    }

    public static void print(Magazine magazine) {
        System.out.println(magazine.getName());
        System.out.println(magazine.getSubject());
        System.out.println(magazine.getPublishing());
        System.out.println(magazine.getMonth());
        System.out.println(magazine.getYear());
    }

    public static void print(Almanac almanac) {
        System.out.println(almanac.getName());
        System.out.println(almanac.getSubject());
        System.out.println(almanac.getPublishing());
        System.out.println(almanac.getYear());
    }
}
