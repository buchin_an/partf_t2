package com.company.model;

public class Almanac {
    private String name;
    private String subject;
    private String publishing;
    private int year;

    public Almanac(String name, String subject, String publishing, int year) {
        this.name = name;
        this.subject = subject;
        this.publishing = publishing;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getSubject() {
        return subject;
    }

    public String getPublishing() {
        return publishing;
    }

    public int getYear() {
        return year;
    }
}
