package com.company.model;

public class Book {

    private String name;
    private String author;
    private String publishing;
    private int year;

    public Book(String name, String author, String publishing, int year) {
        this.name = name;
        this.author = author;
        this.publishing = publishing;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublishing() {
        return publishing;
    }

    public int getYear() {
        return year;
    }
}
