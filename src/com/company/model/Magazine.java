package com.company.model;

import java.util.Calendar;

public class Magazine {
    private String name;
    private String subject;
    private String publishing;
    private int year;
    private int month;

    public Magazine(String name, String subject, String publishing, int year, int month) {
        this.name = name;
        this.subject = subject;
        this.publishing = publishing;
        this.year = year;
        this.month = month;
    }

    public String getName() {
        return name;
    }

    public String getSubject() {
        return subject;
    }

    public String getPublishing() {
        return publishing;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }
}
