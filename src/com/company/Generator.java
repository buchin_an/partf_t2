package com.company;

import com.company.model.Almanac;
import com.company.model.Book;
import com.company.model.Magazine;

public class Generator {
    public static Book getBook() {
        return new Book(Randomizer.getTitle(), Randomizer.getAuthor(), Randomizer.getPublishing(), Randomizer.getYear());
    }

    public static Magazine getMagazine() {
        return new Magazine(Randomizer.getTitle(), Randomizer.getTitle(), Randomizer.getPublishing(), Randomizer.getYear(), Randomizer.getMonth());
    }

    public static Almanac getAlmanac() {
        return new Almanac(Randomizer.getTitle(), Randomizer.getTitle(), Randomizer.getPublishing(), Randomizer.getYear());
    }
}
