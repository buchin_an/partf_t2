package com.company;

import com.company.model.Almanac;
import com.company.model.Book;
import com.company.model.Magazine;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Library library = new Library();
        library.build();
        System.out.println("Enter year");
        int year = Integer.valueOf(scanner.nextLine());

        for (Book book : library.books) {
            if (year == book.getYear()) {
                Printer.print(book);
            }
        }
        for (Magazine magazine : library.magazines) {
            if (magazine.getYear() == year) {
                Printer.print(magazine);
            }
        }
        for (Almanac almanac : library.almanacs) {
            if (almanac.getYear() == year) {
                Printer.print(almanac);
            }
        }


    }
}
