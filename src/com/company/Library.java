package com.company;

import com.company.model.Almanac;
import com.company.model.Book;
import com.company.model.Magazine;

public class Library {
    private final static int BOOK_COUNT = 5;
    private final static int MAGAZINE_COUNT = 5;
    private final static int ALMANAC_COUNT = 5;
    public Book[] books;
    public Magazine[] magazines;
    public Almanac[] almanacs;


    public void build() {
        books = new Book[BOOK_COUNT];
        magazines = new Magazine[MAGAZINE_COUNT];
        almanacs = new Almanac[ALMANAC_COUNT];

        for (int i = 0; i < books.length; i++) {
            books[i] = Generator.getBook();
        }
        for (int i = 0; i < magazines.length; i++) {
            magazines[i] = Generator.getMagazine();
        }
        for (int i = 0; i < almanacs.length; i++) {
            almanacs[i] = Generator.getAlmanac();
        }

    }
}
